<?php
/**
 * Displays footer contacts
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-contacts">
    <div class="block">
        <span class="mob_hide"><?php _e( 'Call Us', 'twentyseventeen' ); ?>: </span><a href="tel:0987654321" class="mob_hide">0987654321</a>
        <a href="tel:0987654321" class="mob_btn"><?php _e( 'Call Us', 'twentyseventeen' ); ?></a>
    </div>
    <div class="block">
        <span class="mob_hide"><?php _e( 'Email', 'twentyseventeen' ); ?>: </span><a href="mailto:testdomain@mail.to" class="mob_hide">testdomain@mail.to</a>
        <a href="mailto:testdomain@mail.to" class="mob_btn"><?php _e( 'Email', 'twentyseventeen' ); ?></a>
    </div>
    <div class="block">
        <a href="#" class="contactUs"><?php _e( 'Contact Us', 'twentyseventeen' ); ?></a>
    </div>
</div><!-- .site-info -->
