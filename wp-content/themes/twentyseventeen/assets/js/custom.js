jQuery(document).ready(function(){

    var popup = jQuery('.popUp');
    var body = jQuery('body');

    popup.css('width', jQuery(window).width() + 17);
    popup.css('height', jQuery(window).height());

    jQuery(window).resize(function () {
        popup.css('width', jQuery(window).width());
        popup.css('height', jQuery(window).height());
    });

    jQuery('.site-contacts .block .contactUs').click(function(e){
        e.preventDefault();
        if (!body.hasClass('no-scroll')){
            popup.fadeIn();
            body.addClass('no-scroll');
        }
    });
    jQuery('html').click(function(e) {
        if( !jQuery(e.target).hasClass('popUp-body') && body.hasClass('no-scroll') && !jQuery(e.target).hasClass('contactUs') )
        {
            popup.fadeOut();
            body.removeClass('no-scroll');
        }
    });
});